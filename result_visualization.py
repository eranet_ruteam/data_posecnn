import numpy as np
import scipy.io as sio
from transforms3d.quaternions import quat2mat, mat2quat
import cv2
import os
import argparse

from render import render, load_ply


def do_smth(args):
    images = sorted(os.listdir(args.images_path))
    points = np.loadtxt(args.cloud_path)
    model = load_ply('data/obj_01_big.ply')
    im_size_rgb = (640, 480)
    clip_near = 10  # [mm]
    clip_far = 10000  # [mm]
    model_texture = None
    ambient_weight = 0.8  # Weight of ambient light [0, 1]
    shading = 'phong'
    # print images
    for idx in range(len(images)):
        image = cv2.imread(os.path.join(args.images_path, images[idx]))
        poses = sio.loadmat(os.path.join(args.mat_path, '{:06d}.mat'.format(idx)))['poses']

        K = np.array([[572.4114, 0, 325.2611], [0, 573.57043, 242.04899], [0, 0, 1]])

        # print points.shape

        x3d = np.ones((4, points.shape[0]), dtype=np.float32)
        x3d[0, :] = points[:, 0]
        x3d[1, :] = points[:, 1]
        x3d[2, :] = points[:, 2]

        text_origin = [0, 15]
        for i in xrange(poses.shape[0]):
            RT = np.zeros((3, 4), dtype=np.float32)
            RT[:3, :3] = quat2mat(poses[i, :4])
            RT[:, 3] = poses[i, 4:7]

            rgb = render(model, im_size_rgb, K, RT[:3, :3], RT[:3, 3] * 1000, clip_near, clip_far,
                         texture=model_texture,
                         ambient_weight=ambient_weight, shading=shading, mode='rgb', surf_color=(1, 0, 1))
            tmp = cv2.cvtColor(rgb, cv2.COLOR_BGR2GRAY)
            _, alpha = cv2.threshold(tmp, 0, 127, cv2.THRESH_BINARY)
            alpha = np.array([alpha]*3)
            alpha = alpha.transpose((1, 2, 0))
            print alpha.shape
            alpha = alpha.astype(float) / 255
            print rgb.shape
            foreground = cv2.multiply(alpha, rgb.astype(float))
            background = cv2.multiply(1.0 - alpha, image.astype(float))
            image = np.uint8(cv2.add(foreground, background))

            # x2d = np.matmul(K, np.matmul(RT, x3d))
            # x2d[0, :] = np.divide(x2d[0, :], x2d[2, :])
            # x2d[1, :] = np.divide(x2d[1, :], x2d[2, :])
            #
            # for x, y in np.transpose(x2d[:2], (1, 0)):
            #     # print int(x), int(y)
            #     cv2.circle(image, (int(x), int(y)), 5, (0, 0, 255), -1)
            #
            for line in RT:
                cv2.putText(image, str(line), tuple(text_origin), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 3)
                cv2.putText(image, str(line), tuple(text_origin), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)
                text_origin[1] += 20
        cv2.imwrite(os.path.join(args.out_path, images[idx]), image)

        cv2.imshow('kek', image)
        cv2.waitKey(0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--out_path', help='output folder', default='data/out')
    parser.add_argument('--images_path', help='input images folder', default='data/images')
    parser.add_argument('--mat_path', help='input math folder', default='data/mat')
    parser.add_argument('--cloud_path', help='model point cloud xyz fromat', default='data/points.xyz')
    args = parser.parse_args()

    do_smth(args)
