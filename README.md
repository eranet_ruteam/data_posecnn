** Install CloudCompare:**

	snap install cloudcompare

** Open model of object in CloudCompare and make following  steps:**

	2.1. Find in the Properties parameter Box center. Values X,Y,Z need to be about 0. 
	2.2. Go to Edit->Apply transformation-> Axis, Angle
	2.3. Edit Translation. For example: if in the Properties parameter Box center has X=1555.0 Y=-801.0 Z=518.0 in Translation you need set inverse values X=-1555.0 Y=801.0 Z=-518.0

** Clone SIXD Toolkit:**

	git clone https://github.com/thodan/sixd_toolkit.git

** Install the required python packages:**

	pip install -r requirements.txt

** Install GLFW library:**

	apt-get install libglfw3

** Install Argparse:**

	pip install argparse

** In $ROOT/sixd_toolkit/params/dataset_params.py set:**

	line 16 common_base_path = �path/to/your/models�
	line 22 p['obj_count'] = 1 #for one model
	line 23 p['scene_count'] = 1 #for one scene

** Go to $ROOT/sixd_toolkit/tools:**

	cd $ROOT/sixd_toolkit/tools

** Run script for generating data in SIXD format:**

	git clone python render_train_imgs.py

** Clone script for conversion from  SIXD format to YCB:**

	https://bitbucket.org/eranet_ruteam/data_posecnn/src/master/

** Download background images based on SUNRGBD dataset:**

	https://drive.google.com/open?id=1cs9XUfCQ5cXNcwywCg0AYi2iEf870IHB

** Run script for conversion:**

	python sixd_2_ycb.py --input [path/to/sixd/output/folder] --output [here/will/be/ycb/format] --background [path/to/SUN/images]