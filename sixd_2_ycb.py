import numpy as np
from PIL import Image
import os
import shutil
import random
import yaml
import scipy.io as sio
import argparse


class Converter():
    def __init__(self, src_dir, back_dir, save_dir):
        self.src = src_dir
        self.all_dirs = sorted(os.listdir(src_dir))
        # self.image_dir = os.path.join(src_dir, 'rgb')
        # self.depth_dir = os.path.join(src_dir, 'depth')
        self.back_dir = back_dir

        # Create destination folders
        for dst_dir in self.all_dirs:
            dst = os.path.join(save_dir, dst_dir)
            if not os.path.exists(dst):
                os.makedirs(dst)
        self.save_dir = save_dir

        # gt_file = open(os.path.join(src_dir, 'gt.yml'), 'r')
        # self.gt_data_loaded = yaml.load(gt_file)

        # Some random X, Y translation magic
        intrinsic_matrix = np.array([[572.4114, 0.0, 325.2611],
                                     [0.0, 573.57043, 242.04899],
                                     [0.0, 0.0, 1.0]])
        self.intrinsic_matrix = intrinsic_matrix
        self.intrinsic_matrix_inv = np.linalg.inv(intrinsic_matrix)


        print self.save_dir
        print self.src
        print self.back_dir

    def _create_mask(self, image_name, back_name, z):
        image_path = os.path.join(self.image_dir, '%04d.png' % image_name)
        image = Image.open(image_path)

        image_rgba = image.convert('RGBA')

        delta = 100 * 700 / z
        delta_x = np.random.randint(-delta, delta)
        delta_y = np.random.randint(-delta, delta)

        image_rgba = image_rgba.transform(image_rgba.size, Image.AFFINE, (1, 0, -delta_x, 0, 1, -delta_y))

        image_rgba_array = np.array(image_rgba)
        mask_array = image_rgba_array.copy()[:, :, 1].squeeze()

        mask_array[mask_array != 0] = self.gt_data_loaded[image_name][0]['obj_id']
        image_rgba_array[mask_array == 0] = np.array([0, 0, 0, 0])

        image_rgba = Image.fromarray(image_rgba_array)
        maks_save = Image.fromarray(mask_array)

        back_path = os.path.join(self.back_dir, back_name)
        back = Image.open(back_path)
        back = back.resize((640, 480), resample=Image.BILINEAR)
        back.paste(image_rgba, mask=image_rgba)
        image_save = back
        # image_save.show()

        maks_save.save(os.path.join(self.dst, '%04d-label.png' % image_name))
        image_save.save(os.path.join(self.dst, '%04d-color.png' % image_name))

        return delta_x, delta_y

    def _create_mat(self, name, d_x, d_y, t):
        center_old = np.dot(self.intrinsic_matrix, t)
        center_old = center_old / center_old[-1]
        center_new = np.array([center_old[0] + d_x, center_old[1] + d_y, 1])
        z = t[-1]
        cent_d = center_new * z
        t = np.dot(self.intrinsic_matrix_inv, cent_d)

        print 'translation'
        print t

        R = np.array(self.gt_data_loaded[name][0]['cam_R_m2c'])
        R = R.reshape((3, 3))
        t = t.reshape((3, 1)) / 1000.0
        RT = np.concatenate((R, t), axis=1)
        poses = np.expand_dims(RT, 2)

        cls_indexes = np.array(self.gt_data_loaded[name][0]['obj_id'])
        cls_indexes = np.expand_dims(cls_indexes, 0)

        center = center_new[:2]
        center = np.expand_dims(center, 0)

        print 'center'
        print center

        factor_depth = np.array([[1.0]])

        mat_data = {'cls_indexes': cls_indexes, 'center': center, 'poses': poses,
                    'intrinsic_matrix': self.intrinsic_matrix, 'factor_depth': factor_depth}
        sio.savemat(os.path.join(self.dst, '%04d-meta.mat' % name), mat_data)

    def _copy_depth(self, name, d_x, d_y):
        src_depth = os.path.join(self.depth_dir, '%04d.png' % name)
        dst_depth = os.path.join(self.dst, '%04d-depth.png' % name)

        depth_map = Image.open(src_depth)
        depth_map = depth_map.transform(depth_map.size, Image.AFFINE, (1, 0, -d_x, 0, 1, -d_y))
        depth_map.save(dst_depth)

    def convert(self):
        backs = os.listdir(self.back_dir)
        train_file = open(os.path.join(self.save_dir, 'train.txt'), 'w')
        val_file = open(os.path.join(self.save_dir, 'val.txt'), 'w')

        for folder in self.all_dirs:
            gt_file = open(os.path.join(self.src, folder, 'gt.yml'), 'r')
            gt_data_loaded = yaml.load(gt_file)
            self.gt_data_loaded = gt_data_loaded
            names = self.gt_data_loaded.keys()

            self.dst = os.path.join(self.save_dir, folder)
            self.depth_dir = os.path.join(self.src, folder, 'depth')
            self.image_dir = os.path.join(self.src, folder, 'rgb')

            train = random.sample(names, int(0.8 * len(names)))
            print len(train)
            # names = names[:1]

            for name in names:
                print 'Now converting %d pic' % name

                if name in train:
                    train_file.write('{}/{:04d}\n'.format(folder, name))
                else:
                    val_file.write('{}/{:04d}\n'.format(folder, name))

                back_name = random.choice(backs)

                t = np.array(self.gt_data_loaded[name][0]['cam_t_m2c'])
                z = t[-1]

                d_x, d_y = self._create_mask(name, back_name, z)
                self._create_mat(name, d_x, d_y, t)
                self._copy_depth(name, d_x, d_y)

        train_file.close()
        val_file.close()


def _main(args):
    conv = Converter(args.input, args.background, args.output)
    conv.convert()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Lets convert some data')
    parser.add_argument('--input', type=str, help='path/to/sixd/folder')
    parser.add_argument('--output', type=str, help='here/will/be/ycb/format')
    parser.add_argument('--background', type=str, help='path/to/SUN/images')

    args = parser.parse_args()
    _main(args)
