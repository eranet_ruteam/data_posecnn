from glumpy import app, gloo, gl
import numpy as np
app.use('glfw')

# Color vertex shader
#-------------------------------------------------------------------------------
_color_vertex_code = """
uniform mat4 u_mv;
uniform mat4 u_nm;
uniform mat4 u_mvp;
uniform vec3 u_light_eye_pos;

attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec3 a_color;
attribute vec2 a_texcoord;

varying vec3 v_color;
varying vec2 v_texcoord;
varying vec3 v_eye_pos;
varying vec3 v_L;
varying vec3 v_normal;

void main() {
    gl_Position = u_mvp * vec4(a_position, 1.0);
    v_color = a_color;
    v_texcoord = a_texcoord;
    v_eye_pos = (u_mv * vec4(a_position, 1.0)).xyz; // Vertex position in eye coords.
    v_L = normalize(u_light_eye_pos - v_eye_pos); // Vector to the light
    v_normal = normalize(u_nm * vec4(a_normal, 1.0)).xyz; // Normal in eye coords.
}
"""

# Color fragment shader - flat shading
#-------------------------------------------------------------------------------
_color_fragment_flat_code = """
uniform float u_light_ambient_w;
uniform sampler2D u_texture;
uniform int u_use_texture;

varying vec3 v_color;
varying vec2 v_texcoord;
varying vec3 v_eye_pos;
varying vec3 v_L;

void main() {
    // Face normal in eye coords.
    vec3 face_normal = normalize(cross(dFdx(v_eye_pos), dFdy(v_eye_pos)));

    float light_diffuse_w = max(dot(normalize(v_L), normalize(face_normal)), 0.0);
    float light_w = u_light_ambient_w + light_diffuse_w;
    if(light_w > 1.0) light_w = 1.0;

    if(bool(u_use_texture)) {
        gl_FragColor = vec4(light_w * texture2D(u_texture, v_texcoord));
    }
    else {
        gl_FragColor = vec4(light_w * v_color, 1.0);
    }
}
"""

# Color fragment shader - Phong shading
#-------------------------------------------------------------------------------
_color_fragment_phong_code = """
uniform float u_light_ambient_w;
uniform sampler2D u_texture;
uniform int u_use_texture;

varying vec3 v_color;
varying vec2 v_texcoord;
varying vec3 v_eye_pos;
varying vec3 v_L;
varying vec3 v_normal;

void main() {
    float light_diffuse_w = max(dot(normalize(v_L), normalize(v_normal)), 0.0);
    float light_w = u_light_ambient_w + light_diffuse_w;
    if(light_w > 1.0) light_w = 1.0;

    if(bool(u_use_texture)) {
        gl_FragColor = vec4(light_w * texture2D(u_texture, v_texcoord));
    }
    else {
        gl_FragColor = vec4(light_w * v_color, 1.0);
    }
}
"""


# Model-view matrix
def _compute_model_view(model, view):
    return np.dot(model, view)

# Model-view-projection matrix
def _compute_model_view_proj(model, view, proj):
    return np.dot(np.dot(model, view), proj)

# Normal matrix (Ref: http://www.songho.ca/opengl/gl_normaltransform.html)
def _compute_normal_matrix(model, view):
    return np.linalg.inv(np.dot(model, view)).T

def _compute_calib_proj(K, x0, y0, w, h, nc, fc, window_coords='y_down'):
    """
    :param K: Camera matrix.
    :param x0, y0: The camera image origin (normally (0, 0)).
    :param w: Image width.
    :param h: Image height.
    :param nc: Near clipping plane.
    :param fc: Far clipping plane.
    :param window_coords: 'y_up' or 'y_down'.
    :return: OpenGL projection matrix.
    """
    depth = float(fc - nc)
    q = -(fc + nc) / depth
    qn = -2 * (fc * nc) / depth

    # Draw our images upside down, so that all the pixel-based coordinate
    # systems are the same
    if window_coords == 'y_up':
        proj = np.array([
            [2 * K[0, 0] / w, -2 * K[0, 1] / w, (-2 * K[0, 2] + w + 2 * x0) / w, 0],
            [0, -2 * K[1, 1] / h, (-2 * K[1, 2] + h + 2 * y0) / h, 0],
            [0, 0, q, qn],  # This row is standard glPerspective and sets near and far planes
            [0, 0, -1, 0]
        ])  # This row is also standard glPerspective

    # Draw the images right side up and modify the projection matrix so that OpenGL
    # will generate window coords that compensate for the flipped image coords
    else:
        assert window_coords == 'y_down'
        proj = np.array([
            [2 * K[0, 0] / w, -2 * K[0, 1] / w, (-2 * K[0, 2] + w + 2 * x0) / w, 0],
            [0, 2 * K[1, 1] / h, (2 * K[1, 2] - h + 2 * y0) / h, 0],
            [0, 0, q, qn],  # This row is standard glPerspective and sets near and far planes
            [0, 0, -1, 0]
        ])  # This row is also standard glPerspective
    return proj.T


def draw_color(shape, vertex_buffer, index_buffer, texture, mat_model, mat_view,
               mat_proj, ambient_weight, bg_color, shading):
    # Set shader for the selected shading
    if shading == 'flat':
        color_fragment_code = _color_fragment_flat_code
    else:  # 'phong'
        color_fragment_code = _color_fragment_phong_code

    program = gloo.Program(_color_vertex_code, color_fragment_code)
    program.bind(vertex_buffer)
    program['u_light_eye_pos'] = [0, 0, 0]  # Camera origin
    program['u_light_ambient_w'] = ambient_weight
    program['u_mv'] = _compute_model_view(mat_model, mat_view)
    program['u_nm'] = _compute_normal_matrix(mat_model, mat_view)
    program['u_mvp'] = _compute_model_view_proj(mat_model, mat_view, mat_proj)
    if texture is not None:
        program['u_use_texture'] = int(True)
        program['u_texture'] = texture
    else:
        program['u_use_texture'] = int(False)
        program['u_texture'] = np.zeros((1, 1, 4), np.float32)

    # Frame buffer object
    color_buf = np.zeros((shape[0], shape[1], 4), np.float32).view(gloo.TextureFloat2D)
    depth_buf = np.zeros((shape[0], shape[1]), np.float32).view(gloo.DepthTexture)
    fbo = gloo.FrameBuffer(color=color_buf, depth=depth_buf)
    fbo.activate()

    # OpenGL setup
    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glClearColor(bg_color[0], bg_color[1], bg_color[2], bg_color[3])
    gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
    gl.glViewport(0, 0, shape[1], shape[0])

    # gl.glEnable(gl.GL_BLEND)
    # gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
    # gl.glHint(gl.GL_LINE_SMOOTH_HINT, gl.GL_NICEST)
    # gl.glHint(gl.GL_POLYGON_SMOOTH_HINT, gl.GL_NICEST)
    # gl.glDisable(gl.GL_LINE_SMOOTH)
    # gl.glDisable(gl.GL_POLYGON_SMOOTH)
    # gl.glEnable(gl.GL_MULTISAMPLE)

    # Keep the back-face culling disabled because of objects which do not have
    # well-defined surface (e.g. the lamp from the dataset of Hinterstoisser)
    gl.glDisable(gl.GL_CULL_FACE)
    # gl.glEnable(gl.GL_CULL_FACE)
    # gl.glCullFace(gl.GL_BACK) # Back-facing polygons will be culled

    # Rendering
    program.draw(gl.GL_TRIANGLES, index_buffer)

    # Retrieve the contents of the FBO texture
    rgb = np.zeros((shape[0], shape[1], 4), dtype=np.float32)
    gl.glReadPixels(0, 0, shape[1], shape[0], gl.GL_RGBA, gl.GL_FLOAT, rgb)
    rgb.shape = shape[0], shape[1], 4
    rgb = rgb[::-1, :]
    rgb = np.round(rgb[:, :, :3] * 255).astype(np.uint8)  # Convert to [0, 255]

    fbo.deactivate()

    return rgb


def render(model, im_size, K, R, t, clip_near=100, clip_far=2000,
           texture=None, surf_color=None, bg_color=(0.0, 0.0, 0.0, 0.0),
           ambient_weight=0.5, shading='flat', mode='rgb+depth'):
    # Process input data
    # ---------------------------------------------------------------------------
    # Make sure vertices and faces are provided in the model
    assert ({'pts', 'faces'}.issubset(set(model.keys())))

    # Set texture / color of vertices
    if texture is not None:
        if texture.max() > 1.0:
            texture = texture.astype(np.float32) / 255.0
        texture = np.flipud(texture)
        texture_uv = model['texture_uv']
        colors = np.zeros((model['pts'].shape[0], 3), np.float32)
    else:
        texture_uv = np.zeros((model['pts'].shape[0], 2), np.float32)
        if not surf_color:
            if 'colors' in model.keys():
                assert (model['pts'].shape[0] == model['colors'].shape[0])
                colors = model['colors']
                if colors.max() > 1.0:
                    colors /= 255.0  # Color values are expected in range [0, 1]
            else:
                colors = np.ones((model['pts'].shape[0], 3), np.float32) * 0.5
        else:
            colors = np.tile(list(surf_color) + [1.0], [model['pts'].shape[0], 1])

    # Set the vertex data
    if mode == 'depth':
        vertices_type = [('a_position', np.float32, 3),
                         ('a_color', np.float32, colors.shape[1])]
        vertices = np.array(zip(model['pts'], colors), vertices_type)
    else:
        if shading == 'flat':
            vertices_type = [('a_position', np.float32, 3),
                             ('a_color', np.float32, colors.shape[1]),
                             ('a_texcoord', np.float32, 2)]
            vertices = np.array(zip(model['pts'], colors, texture_uv),
                                vertices_type)
        else:  # shading == 'phong'
            vertices_type = [('a_position', np.float32, 3),
                             ('a_normal', np.float32, 3),
                             ('a_color', np.float32, colors.shape[1]),
                             ('a_texcoord', np.float32, 2)]
            vertices = np.array(zip(model['pts'], model['normals'],
                                    colors, texture_uv), vertices_type)

    # Rendering
    # ---------------------------------------------------------------------------
    render_rgb = mode in ['rgb', 'rgb+depth']
    render_depth = mode in ['depth', 'rgb+depth']

    # Model matrix
    mat_model = np.eye(4, dtype=np.float32)  # From object space to world space

    # View matrix (transforming also the coordinate system from OpenCV to
    # OpenGL camera space)
    mat_view = np.eye(4, dtype=np.float32)  # From world space to eye space
    mat_view[:3, :3], mat_view[:3, 3] = R, t.squeeze()
    yz_flip = np.eye(4, dtype=np.float32)
    yz_flip[1, 1], yz_flip[2, 2] = -1, -1
    mat_view = yz_flip.dot(mat_view)  # OpenCV to OpenGL camera system
    mat_view = mat_view.T  # OpenGL expects column-wise matrix format

    # Projection matrix
    mat_proj = _compute_calib_proj(K, 0, 0, im_size[0], im_size[1], clip_near, clip_far)

    # Create buffers
    vertex_buffer = vertices.view(gloo.VertexBuffer)
    index_buffer = model['faces'].flatten().astype(np.uint32).view(gloo.IndexBuffer)

    # Create window
    # config = app.configuration.Configuration()
    # Number of samples used around the current pixel for multisample
    # anti-aliasing (max is 8)
    # config.samples = 8
    # config.profile = "core"
    # window = app.Window(config=config, visible=False)
    window = app.Window(visible=False)

    global rgb, depth
    rgb = None
    depth = None

    @window.event
    def on_draw(dt):
        window.clear()
        shape = (im_size[1], im_size[0])
        if render_rgb:
            # Render color image
            global rgb
            rgb = draw_color(shape, vertex_buffer, index_buffer, texture, mat_model,
                             mat_view, mat_proj, ambient_weight, bg_color, shading)
        if render_depth:
            # Render depth image
            global depth
            depth = draw_depth(shape, vertex_buffer, index_buffer, mat_model,
                               mat_view, mat_proj)

    app.run(framecount=0)  # The on_draw function is called framecount+1 times
    window.close()

    # Set output
    # ---------------------------------------------------------------------------
    if mode == 'rgb':
        return rgb
    elif mode == 'depth':
        return depth
    elif mode == 'rgb+depth':
        return rgb, depth
    else:
        print('Error: Unknown rendering mode.')
        exit(-1)


def load_ply(path):
    """
    Loads a 3D mesh model from a PLY file.

    :param path: Path to a PLY file.
    :return: The loaded model given by a dictionary with items:
    'pts' (nx3 ndarray), 'normals' (nx3 ndarray), 'colors' (nx3 ndarray),
    'faces' (mx3 ndarray) - the latter three are optional.
    """
    f = open(path, 'r')

    n_pts = 0
    n_faces = 0
    face_n_corners = 3 # Only triangular faces are supported
    pt_props = []
    face_props = []
    is_binary = False
    header_vertex_section = False
    header_face_section = False

    # Read header
    while True:
        line = f.readline().rstrip('\n').rstrip('\r') # Strip the newline character(s)
        if line.startswith('element vertex'):
            n_pts = int(line.split()[-1])
            header_vertex_section = True
            header_face_section = False
        elif line.startswith('element face'):
            n_faces = int(line.split()[-1])
            header_vertex_section = False
            header_face_section = True
        elif line.startswith('element'): # Some other element
            header_vertex_section = False
            header_face_section = False
        elif line.startswith('property') and header_vertex_section:
            # (name of the property, data type)
            pt_props.append((line.split()[-1], line.split()[-2]))
        elif line.startswith('property list') and header_face_section:
            elems = line.split()
            if elems[-1] == 'vertex_indices':
                # (name of the property, data type)
                face_props.append(('n_corners', elems[2]))
                for i in range(face_n_corners):
                    face_props.append(('ind_' + str(i), elems[3]))
            else:
                print('Warning: Not supported face property: ' + elems[-1])
        elif line.startswith('format'):
            if 'binary' in line:
                is_binary = True
        elif line.startswith('end_header'):
            break

    # Prepare data structures
    model = {}
    model['pts'] = np.zeros((n_pts, 3), np.float)
    if n_faces > 0:
        model['faces'] = np.zeros((n_faces, face_n_corners), np.float)

    pt_props_names = [p[0] for p in pt_props]
    is_normal = False
    if {'nx', 'ny', 'nz'}.issubset(set(pt_props_names)):
        is_normal = True
        model['normals'] = np.zeros((n_pts, 3), np.float)

    is_color = False
    if {'red', 'green', 'blue'}.issubset(set(pt_props_names)):
        is_color = True
        model['colors'] = np.zeros((n_pts, 3), np.float)

    is_texture = False
    if {'texture_u', 'texture_v'}.issubset(set(pt_props_names)):
        is_texture = True
        model['texture_uv'] = np.zeros((n_pts, 2), np.float)

    formats = { # For binary format
        'float': ('f', 4),
        'double': ('d', 8),
        'int': ('i', 4),
        'uchar': ('B', 1)
    }

    # Load vertices
    for pt_id in range(n_pts):
        prop_vals = {}
        load_props = ['x', 'y', 'z', 'nx', 'ny', 'nz',
                      'red', 'green', 'blue', 'texture_u', 'texture_v']
        if is_binary:
            for prop in pt_props:
                format = formats[prop[1]]
                val = struct.unpack(format[0], f.read(format[1]))[0]
                if prop[0] in load_props:
                    prop_vals[prop[0]] = val
        else:
            elems = f.readline().rstrip('\n').rstrip('\r').split()
            for prop_id, prop in enumerate(pt_props):
                if prop[0] in load_props:
                    prop_vals[prop[0]] = elems[prop_id]

        model['pts'][pt_id, 0] = float(prop_vals['x'])
        model['pts'][pt_id, 1] = float(prop_vals['y'])
        model['pts'][pt_id, 2] = float(prop_vals['z'])

        if is_normal:
            model['normals'][pt_id, 0] = float(prop_vals['nx'])
            model['normals'][pt_id, 1] = float(prop_vals['ny'])
            model['normals'][pt_id, 2] = float(prop_vals['nz'])

        if is_color:
            model['colors'][pt_id, 0] = float(prop_vals['red'])
            model['colors'][pt_id, 1] = float(prop_vals['green'])
            model['colors'][pt_id, 2] = float(prop_vals['blue'])

        if is_texture:
            model['texture_uv'][pt_id, 0] = float(prop_vals['texture_u'])
            model['texture_uv'][pt_id, 1] = float(prop_vals['texture_v'])

    # Load faces
    for face_id in range(n_faces):
        prop_vals = {}
        if is_binary:
            for prop in face_props:
                format = formats[prop[1]]
                val = struct.unpack(format[0], f.read(format[1]))[0]
                if prop[0] == 'n_corners':
                    if val != face_n_corners:
                        print('Error: Only triangular faces are supported.')
                        print('Number of face corners: ' + str(val))
                        exit(-1)
                else:
                    prop_vals[prop[0]] = val
        else:
            elems = f.readline().rstrip('\n').rstrip('\r').split()
            for prop_id, prop in enumerate(face_props):
                if prop[0] == 'n_corners':
                    if int(elems[prop_id]) != face_n_corners:
                        print('Error: Only triangular faces are supported.')
                        print('Number of face corners: ' + str(int(elems[prop_id])))
                        exit(-1)
                else:
                    prop_vals[prop[0]] = elems[prop_id]

        model['faces'][face_id, 0] = int(prop_vals['ind_0'])
        model['faces'][face_id, 1] = int(prop_vals['ind_1'])
        model['faces'][face_id, 2] = int(prop_vals['ind_2'])

    f.close()

    return model
